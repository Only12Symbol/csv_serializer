﻿using System;
using System.IO;
using System.Linq;
using csv_serializer.DataClasses;
using csv_serializer.Repository;
using csv_serializer.Serializer;

namespace csv_serializer
{
	public class Program
	{
		private static void Main(string[] args)
		{
			var serializer = new CustomPersonCsvSerializer();

            var person = new Person {Id = Guid.NewGuid(), HairStyle = "Nerd", Name = "Maxim"};

			serializer.Serialize(data: person);

            var restoredPerson = serializer.DeSerialize(new StreamReader(serializer.BaseDirectory)).First();

			Console.WriteLine($"Data before serialization: {person.Id} / {person.Name} / {person.HairStyle}.");
			Console.WriteLine($"Data after serialization: {restoredPerson.Id} / {restoredPerson.Name} / {restoredPerson.HairStyle}.");

			var service = new AccountService(new Repository<CustomAccount>(new Logger(), $"{Directory.GetCurrentDirectory()}\\FileNameForTests.csv"));

            service.AddAccount(new CustomAccount {BirthDate = DateTime.MinValue, FirstName = person.Name, LastName = "Meshkov"});

			Console.WriteLine($"Account added. Name is: {service.LatestAddedAccount.FirstName}. Birth date is: {service.LatestAddedAccount.BirthDate?.Date}");

        }
	}
}
