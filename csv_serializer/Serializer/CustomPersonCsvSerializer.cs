﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using csv_serializer.DataClasses;
using CsvHelper;

namespace csv_serializer.Serializer
{
    public class CustomPersonCsvSerializer : ISerializer<Person>
    {
        private ILogger Logger { get; } = new Logger();

        private const string Delimiter = "\t";
        public readonly string BaseDirectory = $"{Directory.GetCurrentDirectory()}\\CustomFileName.csv";

        public void Serialize( string path = null, params Person[] data)
        {
            try
            {
                using var serializer = new CsvWriter(new StreamWriter(path ?? BaseDirectory), CultureInfo.CurrentCulture);
                serializer.Configuration.HasHeaderRecord = true;
                serializer.Configuration.Delimiter = Delimiter;
                serializer.WriteRecords(data);
                serializer.Flush();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                throw;
            }
        }

        //TODO: only for education purpose, since in task we have to use yield return
        public IEnumerable<Person> DeSerialize(string filePath)
        {
            List<Person> persons;

            try
            {
                using var deSerializer = new CsvReader(new StreamReader(filePath), CultureInfo.CurrentCulture);
                deSerializer.Configuration.Delimiter = Delimiter;
                persons = deSerializer.GetRecords<Person>().ToList();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                throw;
            }
            
            foreach (var person in persons)
            {
                yield return person;
            }
        }

        public IEnumerable<Person> DeSerialize(StreamReader reader)
        {
            try
            {
                using var deSerializer = new CsvReader(reader, CultureInfo.CurrentCulture);
                deSerializer.Configuration.Delimiter = Delimiter;
                return deSerializer.GetRecords<Person>().ToList();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                throw;
            }
        }

        public IEnumerable<string> GetErrors()
        {
            return Logger.Errors.Value;
        }
    }
}
