﻿using csv_serializer.DataClasses;

namespace csv_serializer.Repository
{
    public interface IAccountService
    {
        void AddAccount(CustomAccount account);
    }
}
