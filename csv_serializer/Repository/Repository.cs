﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using csv_serializer.DataClasses;
using csv_serializer.Serializer;
using CsvHelper;

namespace csv_serializer.Repository
{
    public class Repository<TClass> : IRepository<TClass> where TClass : class, IAccount
    {
        public ILogger Logger { get; }

        private readonly string _repoLocation;

        public Repository(ILogger logger, string repoLocation)
        {
            Logger = logger ?? new Logger();
            _repoLocation = repoLocation ?? $"{Directory.GetCurrentDirectory()}\\CustomFileName.csv";
        }

        public IEnumerable<TClass> GetAll()
        {
            Logger.Clean();
            try
            {
                using var reader = new CsvReader(new StreamReader(_repoLocation), CultureInfo.CurrentCulture);
                return reader.GetRecords<TClass>().ToList();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                throw;
            }
            
        }

        public TClass GetOne(Func<TClass, bool> predicate)
        {
            Logger.Clean();
            try
            {
                using var reader = new CsvReader(new StreamReader(_repoLocation), CultureInfo.CurrentCulture);
                return reader.GetRecords<TClass>().FirstOrDefault(predicate);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                throw;
            }
        }

        public void Add(TClass item)
        {
            Logger.Clean();
            if (item == null)
            {
                Logger.LogError("Nothing to add, value is null.");
                throw new NullReferenceException("Value is null.");
            }
            try
            {
                using var writer = new CsvWriter(new StreamWriter(_repoLocation), CultureInfo.CurrentCulture);
                writer.WriteRecord(item);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                throw;
            }
        }
    }
}
