﻿using System;
using System.Collections.Generic;
using csv_serializer.DataClasses;
using csv_serializer.Serializer;

namespace csv_serializer.Repository
{
    public interface IRepository<TClass> where TClass : class, IAccount
    {
        public ILogger Logger { get; }
        IEnumerable<TClass> GetAll();
        TClass GetOne(Func<TClass, bool> predicate);
        void Add(TClass item);
    }
}
