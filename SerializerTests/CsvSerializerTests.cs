using System;
using System.Collections.Generic;
using System.Linq;
using csv_serializer.DataClasses;
using csv_serializer.Repository;
using csv_serializer.Serializer;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace SerializerTests
{
    [TestFixture]
    public class CsvSerializerTests
    {
        private readonly IList<CustomAccount> _accounts = new List<CustomAccount>();
        private Mock<IRepository<CustomAccount>> _mockRepo;

        private const string FirstName = "Maxim";
        private const string LastName = "Meshkov";

        [OneTimeSetUp]
        public void Setup()
        {
            for (var i = 0; i < 5; i++)
                _accounts.Add(new CustomAccount { BirthDate = DateTime.MinValue, FirstName = $"{FirstName}{i}", LastName = $"{LastName}{i}" });

            _mockRepo = new Mock<IRepository<CustomAccount>>();
            _mockRepo.Setup(x => x.Logger).Returns(new Logger());
            _mockRepo.Setup(x => x.Add(_accounts.Single(y => y.FirstName == $"{FirstName}0")));
            _mockRepo.Setup(x => x.GetOne(It.Is<Func<CustomAccount, bool>>(y => true))).Returns(GetOne());
            _mockRepo.Setup(x => x.GetAll()).Returns(GetAll);
        }

        [Test]
        public void AccountServiceAddUserCorrectlyTest()
        {
            var account = _accounts.First(y => y.FirstName == $"{FirstName}0");
            var service = new AccountService(_mockRepo.Object);
            service.AddAccount(account);
            service.GetErrors().Should().BeEmpty();
            service.LatestAddedAccount.Should().BeEquivalentTo(account);
        }

        [Test]
        public void AccountServiceBlockYoungUserTest()
        {
            const string errorMessage = "User must be more then 18 years old.";

            var account = new CustomAccount{ BirthDate = DateTime.MaxValue, FirstName = nameof(FirstName), LastName = nameof(LastName) };
            var service = new AccountService(_mockRepo.Object);
            Assert.Throws<Exception>(() => service.AddAccount(account));
            service.GetErrors().Single().Should().Contain(errorMessage);
            service.LatestAddedAccount.Should().BeNull();
        }

        [Test]
        public void AccountServiceBlockNullUserOrUserWithEmptyDateTest()
        {
            const string errorMessage = "Account or its BirthDate cannot be null.";

            var account = new CustomAccount { BirthDate = null, FirstName = nameof(FirstName), LastName = nameof(LastName) };
            var service = new AccountService(_mockRepo.Object);
            Assert.Throws<Exception>(() => service.AddAccount(account));
            service.GetErrors().Single().Should().BeEquivalentTo(errorMessage);
            service.LatestAddedAccount.Should().BeNull();

            Assert.Throws<Exception>(() => service.AddAccount(null));
            service.GetErrors().Single().Should().BeEquivalentTo(errorMessage);
            service.LatestAddedAccount.Should().BeNull();
        }

        [Test]
        public void AccountServiceGetAccountWithPredicateCorrectlyTest()
        {
            var service = new AccountService(_mockRepo.Object);
            service.GetAccount(x => x.FirstName == $"{FirstName}0").Should().BeEquivalentTo(GetOne());
            service.LatestAddedAccount.Should().BeNull();
            service.GetErrors().Should().BeEmpty();
        }

        private CustomAccount GetOne()
        {
            return _accounts.FirstOrDefault(x => x.FirstName == $"{FirstName}0");
        }

        private IEnumerable<CustomAccount> GetAll()
        {
            return _accounts;
        }
    }
}